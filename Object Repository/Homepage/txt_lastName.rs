<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Text input lastname</description>
   <name>txt_lastName</name>
   <tag></tag>
   <elementGuidId>d1944d6a-b043-4c28-9ffd-6c6b8bffc3cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = 'lastname']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>lastname</value>
   </webElementProperties>
</WebElementEntity>
