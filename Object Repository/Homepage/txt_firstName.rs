<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Text input first name</description>
   <name>txt_firstName</name>
   <tag></tag>
   <elementGuidId>e43395fc-da76-40cb-865b-6228f9d9c124</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = 'firstname']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>firstname</value>
   </webElementProperties>
</WebElementEntity>
