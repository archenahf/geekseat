import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.Select

System.setProperty("webdriver.chrome.driver", "chromedriver")

WebDriver driver = new ChromeDriver()

driver.navigate().to(GlobalVariable.url)
driver.findElement(By.name("firstname")).sendKeys(firstName)
driver.findElement(By.name("lastname")).sendKeys(lastName)
if(gender==true) {
	driver.findElement(By.id("sex-0")).click()
}else if(gender==false) {
	driver.findElement(By.id("sex-1")).click()
}else {
	System.println("Input invalid!")
}

if(experience==1) {
	
	driver.findElement(By.id("exp-0")).click()
	
}else if(experience==2) {
	
	driver.findElement(By.id("exp-1")).click()
	
}else if(experience==3) {
	
	driver.findElement(By.id("exp-2")).click()
	
}else if(experience==4) {
	
	driver.findElement(By.id("exp-3")).click()
	
}else if(experience==5) {
	
	driver.findElement(By.id("exp-4")).click()
	
}else if(experience==6) {
	
	driver.findElement(By.id("exp-5")).click()
	
}else if(experience==7) {
	
	driver.findElement(By.id("exp-6")).click()
	
}else {
	System.println("Experience out of range!")
}

driver.findElement(By.id("datepicker")).sendKeys("date")

if(profession==0) {
	driver.findElement(By.id("profession-0")).click()
	
}else if(profession==1) {
	driver.findElement(By.id("profession-1")).click()
	
}else if(profession==2) {
	driver.findElement(By.id("profession-0")).click()
	driver.findElement(By.id("profession-1")).click()
	
} else {
	System.println("Invalid input!")
}

if(toolsUFT==true) {
	
	driver.findElement(By.id("tool-0")).click()
}

if(toolsProtactor==true) {
	
	driver.findElement(By.id("tool-1")).click()
}

if(toolsAutomation==true) {
	
	driver.findElement(By.id("tool-2")).click()
}

Select slcCountry = new Select(driver.findElement(By.id("continents")))
Select slcCommands = new Select(driver.findElement(By.id("selenium_commands")))

slcCountry.selectByVisibleText(Continens)
slcCommands.selectByVisibleText(seleniumCommands)

File file = new File("SS.png")
driver.findElement(By.id("photo")).sendKeys(file.getAbsolutePath())

driver.findElement(By.xpath("//a[@href='https://github.com/stanfy/behave-rest/blob/master/features/conf.yaml']")).click()

driver.navigate().back()
driver.findElement(By.id("submit")).click()

driver.close()




