# geekSeat

#How to run
1. Clone project on katalon studio
2. Select testcase , click ikon run 

#Testcase
1. TC01_HomePage for automation url : https://www.techlistic.com/p/selenium-practice-form.html
2. TC02_findStructure for automation url : https://www.techlistic.com/p/demo-selenium-practice.html

#See script on Folder "Scripts"
If you want to run script on different tools copy file script from folder "Scripts" 
1. Download Scripts folder
2. Download webdriver and put on same folder
3. Download SS.png and put on same folder
